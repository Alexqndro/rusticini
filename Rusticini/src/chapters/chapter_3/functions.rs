fn main() {
    // Statement vs expression
    let a = 6; // statement (no return value)
               // expression (return value) in RUST binding variables does not return the value assigned
               //let x = (let x = 6);  // Wrong
               //let b = (let a = 6);

    let alfa = {
        let a = 3;
        a + 1
    };

    println!("The value of a is: {alfa}, {a}");

    let x = alfa_plus_beta(5, alfa);

    println!("The value of alfa + beta is: {x}");
}

fn alfa_plus_beta(a: i32, b: i32) -> i32 {
    a + b
}
