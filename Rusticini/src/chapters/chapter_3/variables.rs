// Const is always constant and can be defined as a global constand
// The compiler will perform expression at compile time
const HOURS_IN_SECONDS: u32 = 60 * 60 * 3;

fn main() {
    // In Rust variables are immutable by default, use mut to make them mutable
    let mut a = 5;
    println!("The value of x is: {a}");
    a = 6;
    println!("The value of x is: {a}");

    let a_sec = a * HOURS_IN_SECONDS;
    println!("In {a} hours there are {a_sec} seconds");

    // Shadowing
    // Here the variable b is defined as 5, then its shadowed to 6 (it need let)
    // within the inner scope, the variable b is now shadowed to 12
    // after the scope end, the variable b return to its previous value
    let b = 5;

    let b = b + 1;

    {
        let b = b * 2;
        println!("The value of x in the inner scope is: {b}");
    }

    println!("The value of x is: {b}");

    let space = "   ";

    {
        // Shadowed variables can be of different type!
        let space = space.len();
        println!("The number of spaces is: {space}");
    }
    println!("The value space is: _{space}_");
}
