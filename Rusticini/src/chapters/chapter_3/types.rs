use std::io;
fn main() {
    // Overflow test
    //let mut u:i8 = 0;
    //loop {
    //    u = u + 1;
    //    println!("The value u is: {u}");
    //}

    // Compounds
    // Tuplet
    let tup: (i32, f64, u8) = (500, 6.4, 1); // Tup definition
    let (x, y, z) = tup; // Tup destructuring
    println!("The value tup is: {x}, {y}, {z}");
    let tup: (i32, f64, u8) = (590, 7.4, 2);
    let x = tup.0;
    let y = tup.2;
    let z = tup.1;

    println!("The value tup is: {x}, {y}, {z}");

    // Arrays
    let vector = [1, 2, 3, 4, 5];

    // The code will panick (possible error at run time)
    // if we try to access an index > size of vector
    println!("Please enter an array index.");

    let mut index = String::new();

    io::stdin()
        .read_line(&mut index)
        .expect("Failed to read line");

    let index: usize = index
        .trim()
        .parse()
        .expect("Index entered was not a number");

    let element = vector[index];

    println!("The value of the element at index {index} is: {element}");
}
