use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("Playing guess the number");

    /* Part 1:
        - Generate a random number
    */
    // Crate: collection of Rust source code files
    let secret_number = rand::thread_rng().gen_range(1..=100);

    println!("The secret number is: {secret_number}");

    loop {
        // Part 1:
        //     - Ask for an user input
        //     - Process the input
        //     - Verify that the input is in the correct form
        println!("Please input your guess, type quit if you want to leave");
        let mut guess = String::new();

        /* read_line receives an input &mut guess, where guess is a mutable reference &mut
        read_line returns a Result value, which is an enum that can be wither Ok or Err
        we must specify what to do in case of Result value = err */

        // Correct code

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        if guess.trim() == "quit" {
            println!("See you later!");
            break;
        }

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Try with a number");
                continue;
            } // va alla prossima iterazione del loop
        };
        // L'altra variabile guess viene esiliata nel reame delle ombre

        // Code with warning
        /*
        io::stdin()
        .read_line(&mut guess);
        println!("You guessed: {guess}"); // {} placeholder that hold the value inside the var
        */

        /* Part 3:
            - Compare the Guess and Secret number
            - Display Greater, Less or Equal
        */

        // Match: control flow based on pattern matching, ha più arms
        //     ogni arm ha: un pattern che va matchato => il codice che va eseguito in caso di match
        //     cmd: un metodo che compara var1 e var2 e ritorna un ordering
        //     ordering: enum che può essere:
        // - Less
        // - Greater
        // - Equal
        //     match var1.cmp(&var2) {
        //     Ordering::Less => do action
        // }
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"), //
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
